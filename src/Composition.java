
public class Composition extends Record {
	private String name;
	private String type;
	private int duration;

	public Composition() {

		this.name = "Invalid";
		this.id = "Invalid";
		this.type = "saving";
		this.duration = 0;
		this.amount = 0;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	String id;
	int amount;

	public Composition(String name, Account cont) {
		this.name = name;
		this.id = cont.getId();
		this.type = cont.type;
		this.amount = cont.getMoney();
		this.duration=cont.getDuration();

	}

	@Override
	public Object get_stored() {
		// TODO Auto-generated method stub
		Object aux = null;

		switch (this.type) {
		case "saving":
			aux = new SavingAccount(this.id, this.amount, this.duration);
			break;
		case "spending":
			aux = new SpendingAccount(this.id, this.amount, this.duration);
			System.out.println("This type is " + this.type);
			break;
		}
		return aux;
	}

	@Override
	public String toString() {
		return "Owner:" + this.name + " type:" + this.type + " amount:" + this.amount + " duration" + this.duration;
	}

}
