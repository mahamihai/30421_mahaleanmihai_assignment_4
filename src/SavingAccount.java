
public class SavingAccount extends Account {
	
	
	public SavingAccount(String id)
	{
		super(id);
		this.money=0;
		this.duration=0;
		this.type="saving";
	}
	public SavingAccount(String id,int amount,int duration)
	{
		super(id);
		this.money=amount;
		this.type="saving";
		this.duration=duration;
	}
	public int withdraw(int amount)
	{
		
		if(amount != this.getMoney())
		{
		
			System.out.println("cannot withdraw smaller amounts because it is a saving account");
			return 0;
		}
		this.observer.update("Withdrawned:"+amount+" from account:"+this.getId());
		this.setMoney(0);
		return amount;
	}
	
	public void deposit(int amount)
	{
		if(this.getMoney()!=0)
		{
			System.out.println("Cannot deposit money because it account not empty");
			
		}
		else
		{
			this.observer.update("Deposited:"+amount+" from account:"+this.getId());

			this.setMoney(amount);
			
		}
		System.out.println("Deposited:"+this.money);
		
	}

}
