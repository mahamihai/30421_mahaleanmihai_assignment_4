
public abstract class Account implements java.io.Serializable {
	protected final String id;
	protected int money;
	protected Observer observer;
	protected int duration;
	
	public int getDuration() {
		return duration;
	}
	
	public void attach(Observer observer)
	{
		this.observer=observer;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	protected String type = "";

	public String getId() {
		return id;
	}

	public int getMoney() {
		return money;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (duration != other.duration)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (money != other.money)
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	public Account(String id) {
		this.id = id;
		this.setMoney(0);
	}

	public void setMoney(int money) {
		this.money = money;
	}

	@Override
	public String toString() {

		return "Account id:" + this.getId() + " Available amount:" + this.getMoney() + " Account type:"
				+ this.getType()+"duration:"+this.duration;
	}

	public abstract void deposit(int amount);

	public abstract int withdraw(int amount);
}
