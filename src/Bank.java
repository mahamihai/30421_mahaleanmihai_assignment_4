import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Set;
import java.util.UUID;

public class Bank implements BankProc, java.io.Serializable {
	Hashtable<String, LinkedEntry> accounts = new Hashtable<String, LinkedEntry>();
	int all_money;

	public Bank() {
		this.all_money = total_in();
	}
	public Person get_person(String name)
	{
		for(Person aux:this.persons)
		{
			if(aux.getName().equals(name))
			{
				return aux;
			}
		}
		return null;
	}
	public Boolean check_integrity() {
		// System.out.println("All money:"+this.all_money+"
		// computed:"+this.total_in());
		return (all_money == this.total_in());
	}

	public Hashtable<String, LinkedEntry> getAccounts() {
		return accounts;
	}

	public void setAccounts(Hashtable<String, LinkedEntry> accounts) {
		this.accounts = accounts;
		all_money = total_in();
	}

	public int total_in() {
		int amount = 0;

		for (LinkedEntry aux : this.accounts.values()) {
			while (aux != null) {
				amount += aux.getCont().getMoney();
				aux = aux.getNext();
			}

		}
		return amount;
	}

	public int check_and_save() {
		if (this.check_integrity()) {
			try {
				this.create_backup();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return 1;
		} else {
			System.out.println("Error occured, internal structure corrupted");
			try {
				this.restore_backup();
			} catch (ClassNotFoundException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return 0;
		}
	}

	public void deposit(String id, int amount) {

		Account aux = this.read_account(id).getCont();
		System.out.println("Before deposit " + aux.getMoney());
		aux.deposit(amount);
		System.out.println("After deposit " + aux.getMoney());
		all_money += amount;
		check_and_save();
	}

	public void withdraw(String id, int amount) {
		Account aux = this.read_account(id).getCont();
		aux.withdraw(amount);
		all_money -= amount;
		
	}

	public ArrayList<Person> getPersons() {
		return persons;
	}

	public void update_info(Person aux) {
		for (Person tmp : this.persons) {
			if (tmp.getName().equals(aux.getName())) {
				System.out.println(aux.getAge());
				tmp.setAge(aux.getAge());
				break;
			}
		}
		check_and_save();
	}

	public LinkedEntry get_accounts(String name) {
		return accounts.get(name);
	}

	public void setPersons(ArrayList<Person> persons) {
		this.persons = persons;
		check_and_save();
	}

	ArrayList<Person> persons = new ArrayList<Person>();

	public void add_person(String name, int age) {
		assert ! name.contains("./ -*");
		assert age>0 && age <120;
		Person aux = new Person(name, age);
		persons.add(aux);
		check_and_save();
	}

	public void add_person(Person aux) {
		assert ! aux.getName().contains("./ -*");
		assert aux.getAge()>0 && aux.getAge() <120;
		persons.add(aux);
		check_and_save();
	}

	private LinkedEntry create_nod(String type) {
		String id = UUID.randomUUID().toString();
		Account aux = null;
		switch (type) {
		case "saving":
			aux = new SavingAccount(id);
			break;
		case "spending":
			aux = new SpendingAccount(id);
			break;
		}
		LinkedEntry tmp = new LinkedEntry(aux);
		tmp.setNext(null);
		return tmp;

	}

	public Account add_account(String name, String type) {
		assert type.equals("spending") || type.equals("saving");
		assert ! name.contains("./ -*");
		int size=accounts.values().size();
		LinkedEntry entry = create_nod(type);
		LinkedEntry aux = accounts.get(name);
		Observer observer=new Observer();
		observer.setOwner(this.get_person(name));
		entry.getCont().attach(observer);
		if (aux == null) {
			accounts.put(name, entry);

		} else {
			while (aux.getNext() != null) {
				aux = aux.getNext();
			}
			aux.setNext(entry);
		}
		System.out.println(size+" "+accounts.values().size());
		//assert accounts.values().size()>size;
		check_and_save();
		
		return entry.getCont();
	}

	public void remove_person(String name, int age) {
		assert age>0 && age <120;
		assert ! name.contains("./ -*");
		Person aux = new Person(name, age);
		accounts.remove(name);
		persons.remove(aux);
		check_and_save();

	}

	public void remove_person(Person aux) {
		assert ! aux.getName().contains("./ -*");
		assert aux.getAge()>0 && aux.getAge() <120;
		assert persons.contains(aux);
		int size=persons.size();
		accounts.remove(aux.getName());
		persons.remove(aux);
		check_and_save();
		assert persons.size()<size;

	}

	public void remove_account(String id) {

		assert this.read_account(id)!=null;
		
		int size=accounts.values().size();
		Set<String> entries = accounts.keySet();// get all keys
		for (String key : entries) {
			LinkedEntry first = accounts.get(key);// pass through keys and
													// search for account
			if (first != null) {
				if (first.getCont().getId().equals(id))
					first = first.getNext();// if first is the account ignore it
				else {
					LinkedEntry parent = first;
					LinkedEntry looper = first.getNext();
					while (looper != null)// pass through each accoutn in the
											// chain
					{
						if (looper.getCont().getId().equals(id)) {
							all_money -= looper.getCont().getMoney();
							parent.setNext(looper.getNext());
							break;
						}
						parent = looper;
						looper = looper.getNext();
					}

				}
				accounts.put(key, first);
			}

		}
		//assert size>this.accounts.values().size();
		check_and_save();
		

	}

	public LinkedEntry read_account(String id) {
		
		for (String key : accounts.keySet()) {
			LinkedEntry aux = accounts.get(key);
			while (aux != null) {
				if (aux.getCont().getId().equals(id)) {
					assert aux.getClass()==LinkedEntry.class;
					return aux;
				}
				aux = aux.getNext();
			}

		}
		return null;
	}

	public void write_account(String name, Account cont) {
		assert persons.contains(name);
		
		LinkedEntry aux = accounts.get(name);
		assert aux!=null;
		LinkedEntry tmp = new LinkedEntry(cont);
		if (aux != null) {
			while (aux.getNext() != null)
				aux = aux.getNext();

			aux.setNext(tmp);
		} else {
			accounts.put(name, aux);
		}
		all_money += cont.getMoney();
		
		check_and_save();

	}

	@Override
	public String toString() {
		String bank_info = "Bank information";
		for (Person person : this.persons) {
			bank_info += "\n" + person.getName();
			LinkedEntry entry = this.accounts.get(person.getName());
			if (entry != null) {

				bank_info += "\n" + entry.toString();
				while (entry.getNext() != null) {
					entry = entry.getNext();
					bank_info += "\n" + entry.toString();

				}
			}

		}
		return bank_info;

	}

	@SuppressWarnings("unchecked")
	public void restore_backup() throws IOException, ClassNotFoundException {
		FileInputStream fileIn = new FileInputStream("persons.ser");
		ObjectInputStream in = new ObjectInputStream(fileIn);
		this.persons = (ArrayList<Person>) in.readObject();
		in.close();
		fileIn.close();
		fileIn = new FileInputStream("accounts.ser");
		in = new ObjectInputStream(fileIn);
		this.accounts = (Hashtable<String, LinkedEntry>) in.readObject();
		in.close();
		fileIn.close();
		this.all_money = total_in();

	}

	public void create_backup() throws IOException {

		FileOutputStream fileOut = new FileOutputStream("accounts.ser");
		ObjectOutputStream out = new ObjectOutputStream(fileOut);
		out.writeObject(this.accounts);
		out.close();
		fileOut.close();
		fileOut = new FileOutputStream("persons.ser");
		out = new ObjectOutputStream(fileOut);
		out.writeObject(this.persons);
		out.close();
		fileOut.close();

	}

}
