
public class Observer implements java.io.Serializable{
	private Person owner;
	public Person getOwner() {
		return owner;
	}
	public void setOwner(Person owner) {
		this.owner = owner;
	}
	public void update(String aux)
	{
		
		owner.notify(aux);
	}
	public Observer()
	{
		this.owner=null;
	}

}
