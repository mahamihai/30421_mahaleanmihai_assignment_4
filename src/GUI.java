
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.IntrospectionException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class GUI {

	private DefaultTableModel create_model(List<Object> list) {

		int nr_fields = 0;
		// Create a couple of columns
		ArrayList<String> colls = new ArrayList<String>();

		if (list.size() > 0) {
			for (Field field : list.get(0).getClass().getDeclaredFields()) {
				nr_fields++;
				colls.add(field.getName());

			}
		}
		DefaultTableModel model = new DefaultTableModel(colls.toArray(), 0);

		for (Object aux : list) {
			int current = 0;
			Object values[] = new Object[nr_fields];
			for (Field field : aux.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				try {
					String val = field.get(aux).toString();
					values[current] = val;
					current++;
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			model.addRow(values);
		}

		return model;

	}

	private ArrayList<JTextField> create_fields(Class type, JPanel panel) {
		ArrayList<JTextField> display_fields = new ArrayList<JTextField>();
		for (Field field : type.getDeclaredFields()) // add labels to display
														// info for editing

		{
			JLabel tmp = new JLabel(field.getName());
			panel.add(tmp);
			JTextField aux = new JTextField();
			aux.setMinimumSize(new Dimension(100, 30));
			aux.setPreferredSize(new Dimension(100, 30));
			display_fields.add(aux);
			panel.add(aux);

		}
		return display_fields;

	}

	public JTable accounts_t(Bank bank) {
		List<Object> compositions = new ArrayList<Object>();
		List<Person> a_persons = bank.getPersons();
		for (Person aux : a_persons) {
			LinkedEntry tmp = bank.get_accounts(aux.getName());
			while (tmp != null) {
				Account cont = tmp.getCont();
				compositions.add(new Composition(aux.getName(), cont));
				tmp = tmp.getNext();
			}

		}
		return new JTable(create_model(compositions));

	}

	public JTable persons_t(Bank bank) {
		List<Object> a_persons = new ArrayList<Object>(bank.getPersons());

		return new JTable(create_model(a_persons));

	}

	protected void setup_info(ArrayList<JTextField> views, JTable table) {
		int selected = table.getSelectedRow();

		for (int i = 0; i < table.getColumnCount(); i++) {
			views.get(i).setText(table.getValueAt(selected, i).toString());
		}

	}

	private Object create_entry(ArrayList<JTextField> views, Class type) {
		Object aux = null;
		//System.out.println(type.getSimpleName());
		try {
			System.out.println(type.getSimpleName());
			aux = type.newInstance();
		} catch (InstantiationException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IllegalAccessException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		int i = 0;
		for (Field f : type.getDeclaredFields()) {
			System.out.println(f.getName());
			f.setAccessible(true);
			try {
				f.set(aux, Integer.parseInt(views.get(i).getText().toString()));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				try {
					f.set(aux, views.get(i).getText().toString());
				} catch (IllegalArgumentException e1) {
					// TODO Auto-generated catch block

				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block

				}

			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block

			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block

			}
			i++;
		}

		return aux;
	}

	public JPanel persons_panel(Bank bank) {
		Class type = Person.class;
		JTable table = persons_t(bank);
		JPanel panel = new JPanel();
		panel.add(table);
		displays = create_fields(type, panel);
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 1) { // check if a double click

					setup_info(displays, table);

				}
			}
		});

		JButton but = new JButton("Add");
		but.addActionListener(new ActionListener() // Add function
		{
			public void actionPerformed(ActionEvent e) {

				bank.add_person((Person) create_entry(displays, Person.class));
				persons(bank);

			}
		});
		panel.add(but);

		but = new JButton("Update info");
		but.addActionListener(new ActionListener() // Update function
		{
			public void actionPerformed(ActionEvent e) {

				bank.update_info((Person) create_entry(displays, Person.class));
				persons(bank);
			}
		});
		panel.add(but);
		panel.add(but);
		but = new JButton("Delete ");
		but.addActionListener(new ActionListener() // Delete function
		{
			public void actionPerformed(ActionEvent e) {

				bank.remove_person((Person) create_entry(displays, Person.class));
				persons(bank);
			}
		});
		panel.add(but);
		return panel;

	}

	public JPanel accounts_panel(Bank bank) {
		Class type = Composition.class;
		JTable table = accounts_t(bank);
		JPanel panel_t = new JPanel();
		JPanel panel = new JPanel();
		// panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		// panel_t.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		displays = create_fields(type, panel);
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 1) { // check if a double click
					//System.out.println(table.getColumnCount());
					setup_info(displays, table);
				}
			}
		});
		panel_t.add(new JScrollPane(table));
		JButton but = new JButton("Add");
		but.addActionListener(new ActionListener() // add function
		{
			public void actionPerformed(ActionEvent e) {
				Composition aux = (Composition) create_entry(displays, Composition.class);
				bank.add_account(aux.getName(), aux.getType());
				accounts(bank);
			}
		});
		panel.add(but);

		but = new JButton("Update info");
		but.addActionListener(new ActionListener() // Update function
		{
			public void actionPerformed(ActionEvent e) {
				Composition aux = (Composition) create_entry(displays, Composition.class);
				
				LinkedEntry tmp = bank.read_account(aux.getId());
				tmp.setCont((Account) aux.get_stored());
				//System.out.println(tmp.getCont().toString());
				bank.check_and_save();
				System.out.println(bank.toString());
				accounts(bank);

			}
		});
		panel.add(but);
		panel.add(but);
		but = new JButton("Delete ");
		but.addActionListener(new ActionListener() // Delete function
		{
			public void actionPerformed(ActionEvent e) {
				Composition aux = (Composition) create_entry(displays, Composition.class);

				bank.remove_account(aux.getId());
				accounts(bank);

			}
		});
		panel.add(but);
		JPanel panel1 = new JPanel();
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.LINE_AXIS));
		JLabel amount_label = new JLabel("Amount:");
		panel1.add(amount_label);
		JTextField amount = new JTextField("");
		amount.setMinimumSize(new Dimension(100, 30));
		amount.setPreferredSize(new Dimension(100, 30));
		panel1.add(amount);

		but = new JButton("Deposit ");
		but.addActionListener(new ActionListener() // Deposit function
		{
			public void actionPerformed(ActionEvent e) {

				Composition aux = (Composition) create_entry(displays, Composition.class);

				bank.deposit(aux.getId(), Integer.parseInt(amount.getText()));
				accounts(bank);
				

			}
		});
		panel1.add(but);

		but = new JButton("Withdraw ");
		but.addActionListener(new ActionListener() // WIthdraw function
		{
			public void actionPerformed(ActionEvent e) {
				Composition aux = (Composition) create_entry(displays, Composition.class);
			
				bank.withdraw(aux.getId(), Integer.parseInt(amount.getText()));
				//Account tmp=bank.read_account(aux.id).getCont();
				accounts(bank);

			}
		});
		panel1.add(but);

		panel.add(panel1);

		JPanel panel2 = new JPanel();
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.X_AXIS));
		panel2.add(panel_t);
		panel2.add(panel);
		panel2.add(panel1);

		return panel2;

	}

	protected ArrayList<JTextField> displays;
	JTable table;

	public void persons(Bank bank) {

		JPanel panel;
		persons_frame.getContentPane().removeAll();

		panel = persons_panel(bank);

		persons_frame.add(panel);
		persons_frame.setVisible(true);
	}

	public void accounts(Bank bank) {
		JPanel panel;

		accounts_frame.getContentPane().removeAll();
		panel = accounts_panel(bank);

		accounts_frame.add(panel);
		accounts_frame.setVisible(true);
	}

	JFrame persons_frame = new JFrame("Persons");
	JFrame accounts_frame = new JFrame("Accounts");

	public void setup_gui(Bank bank) {
		accounts_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		accounts_frame.setSize(1000, 600);
		accounts_frame.setLocation(300, 300); // setup the frame dimensions
		persons_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		persons_frame.setSize(1000, 600);
		persons_frame.setLocation(300, 300); // set
		JFrame main = new JFrame();
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		;
		main = new JFrame("Bank");
		main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		main.setSize(1000, 500);
		main.setLocation(300, 300); // setup the fram

		JButton but = new JButton("Accounts");
		but.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accounts(bank);

			}
		});
		panel.add(but);
		but = new JButton("Persons");
		but.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				persons(bank);
			}
		});
		panel.add(but);

		main.add(panel);
		main.setVisible(true);

	}

}
