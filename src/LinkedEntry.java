
public class LinkedEntry implements java.io.Serializable {
	private Account cont;

	public Account getCont() {
		return cont;
	}

	public void setCont(Account cont) {
		this.cont = cont;
	}

	private LinkedEntry next;

	public LinkedEntry getNext() {
		return next;
	}

	public void setNext(LinkedEntry next) {
		this.next = next;
	}

	public LinkedEntry(Account cont) {
		this.next = null;
		this.cont = cont;
	}

	@Override
	public String toString() {
		Account aux = this.cont;
		return "Account id:" + aux.getId() + " Available amount:" + aux.getMoney() + " Account type:" + aux.getType()+" Duration:"+aux.getDuration();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cont == null) ? 0 : cont.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LinkedEntry other = (LinkedEntry) obj;
		if (cont == null) {
			if (other.cont != null)
				return false;
		} else if (!cont.equals(other.cont))
			return false;
		return true;
	}

}
