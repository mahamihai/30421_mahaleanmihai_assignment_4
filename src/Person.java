
public class Person extends Record implements java.io.Serializable  {

	private String name;
	public Person(String name,int age)
	{
		this.name=name;
		this.age=age;
	}
	public void notify(String aux)
	{
		System.out.println("Person's "+this.getName()+" account state has been changed>"+aux);
	}
	public Person()
	{
		this.age=-1;
		this.name="Invalid";
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	private int age;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (age != other.age)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	@Override
	public Object get_stored() {
		// TODO Auto-generated method stub
		return this;
	}
	
}
