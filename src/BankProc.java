
public interface BankProc {

	/**
	 * 
	 * @param name must be real
	 * @param age >0 and <120
	 * @return persons array must increase
	 */
	public void add_person(String name,int age);
	/**
	 * 
	 * @param name must be a real name
	 * @param age >0 and <120
	 * @return persons array decrease
	 */
	
	public void remove_person(String name,int age);
	/**
	 * 
	 * @param name the persons real name
	 * @param type must be "saving" or "spending"
	 * @return accounts array must increase
	 */
	public Account add_account(String name,String type);
	/**
	 * 
	 * @param id must represent an valid id of a prevoiusly sotred account
	 */
	public void remove_account(String id);
	/**
	 * 
	 * @param id must represent a real array
	 * @return must return an linkedEntry 
	 */
	public LinkedEntry read_account(String id);
	/**
	 * 
	 * @param name must be an actual client
	 * @param cont must be an valid id representing an existing account
	 * 
	 */
	public void write_account(String name, Account cont);
	/**
	 * 
	 */
	
}
