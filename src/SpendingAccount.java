
public class SpendingAccount extends Account {

	public int withdraw(int amount)
	{
		if(this.getMoney()<amount)
		{
			System.out.println("Too much money to withdraw");
			return 0;
		}
		this.observer.update("Withdrawned:"+amount+" from account:"+this.getId());

		this.setMoney(this.getMoney()-amount);
		this.observer.update("Withdrawned:"+amount+" from account:"+this.getId());

		return amount;
	}
	public SpendingAccount(String id)
	{
		super(id);
		this.type="spending";
	}
	
	
	public SpendingAccount(String id,int amount,int duration)
	{
	super(id);
	this.money=amount;
	this.type="spending";
	this.duration=duration;
    }
	public void deposit(int amount)
	{
		this.observer.update("Deposite:"+amount+" from account:"+this.getId());

		this.setMoney(this.getMoney()+amount);
	}
	
}
